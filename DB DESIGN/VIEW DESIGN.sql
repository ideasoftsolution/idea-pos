CREATE VIEW `vShowAllEmployee` as
SELECT 
  A.EmployeeID,
  A.EmployeeCompanyID,
  A.EmployeeCompanyIDNum,
  A.EmployeeTitleID,
  A.IDNum,
  A.IDCat,
  A.FullName,
  A.NickName,
  A.BirthPlace,
  A.BirthDay,
  A.Gender,
  A.Phone,
  A.Email,
  A.AddressLine1,
  A.AddressLine2,
  A.City,
  A.PostalCode,
  A.Province,
  A.Country,
  A.SignInDate,
  A.SignOutDate,
  A.Active,
  B.EmployeeTitleName,
  C.AppUserID,
  C.AppUserName,
  C.AppUserPassword,
  C.AppUserDesc,
  C.DefaultUser
  FROM Employee A
  INNER JOIN EmployeeTitle B ON A.EmployeeTitleID = B.EmployeeTitleID
  LEFT JOIN AppUser C ON A.AppUserID = C.AppUserID
  ORDER BY A.FullName;