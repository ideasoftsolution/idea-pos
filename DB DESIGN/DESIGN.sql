CREATE TABLE EmployeeTitle
(
  EmployeeTitleID smallint NOT NULL AUTO_INCREMENT,
  EmployeeTitleName varchar(32) NOT NULL DEFAULT '',
  Active tinyint(1) NOT NULL DEFAULT 1,
  CONSTRAINT PKEmployeeTitle PRIMARY KEY(EmployeeTitleID),
  CONSTRAINT UQEmployeeTitle UNIQUE(EmployeeTitleName)
);

INSERT INTO EmployeeTitle
  VALUES(1, 'Supervisor', 1);

CREATE TABLE AppUser
(
  AppUserID int NOT NULL AUTO_INCREMENT,
  AppUserName varchar(32) NOT NULL,
  AppUserPassword varchar(96) NOT NULL,
  AppUserDesc varchar(64) NOT NULL DEFAULT '' COMMENT 'Title/Role',
  DefaultUser tinyint(1) NOT NULL DEFAULT 0,
  Active tinyint(1) NOT NULL DEFAULT 1,
  CONSTRAINT PKAppUser PRIMARY KEY (AppUserID),
  CONSTRAINT UQAppUser UNIQUE (AppUserName)
);

INSERT INTO AppUser
  VALUES (1, 'admin', md5('123456'), 'Super Admin', 1, 1);

CREATE TABLE Employee
(
  EmployeeID int NOT NULL AUTO_INCREMENT,
  EmployeeCompanyID varchar(32) NOT NULL COMMENT 'NIK Karyawan',
  EmployeeCompanyIDNum int NOT NULL DEFAULT 1 COMMENT 'Kolom untuk AUTONUMBER EmployeeCompanyID',
  EmployeeTitleID smallint NOT NULL COMMENT 'Jabatan Karyawan',
  AppUserID int NULL DEFAULT NULL COMMENT 'ID User',
  IDNum varchar(48) NOT NULL DEFAULT '' COMMENT 'NO KTP/SIM/PASPOR',
  IDCat varchar(48) NOT NULL DEFAULT '' COMMENT 'KTP/SIM/PASPOR',
  FullName varchar(64) NOT NULL DEFAULT '',
  NickName varchar(32) NOT NULL DEFAULT '',
  BirthPlace varchar(64) NOT NULL DEFAULT '',
  BirthDay datetime NOT NULL DEFAULT '0000-00-00',
  Gender tinyint(1) NOT NULL DEFAULT 1,
  Phone varchar(16) NOT NULL DEFAULT '',
  Email varchar(32) NOT NULL DEFAULT '',
  AddressLine1 varchar(64) NOT NULL DEFAULT '',
  AddressLine2 varchar(64) NOT NULL DEFAULT '',
  City varchar(32) NOT NULL DEFAULT '',
  PostalCode varchar(9) NOT NULL DEFAULT '',
  Province varchar(32) NOT NULL DEFAULT '',
  Country varchar(32) NOT NULL DEFAULT '',
  SignInDate datetime NOT NULL DEFAULT '0000-00-00',
  SignOutDate datetime NULL DEFAULT NULL,
  Active tinyint(1) NOT NULL DEFAULT 1,
  CONSTRAINT PKEmployee PRIMARY KEY(EmployeeID),
  CONSTRAINT UQEmployee UNIQUE (EmployeeCompanyID),
  CONSTRAINT FKEmployeeRefAppUser FOREIGN KEY(AppUserID) REFERENCES AppUser(AppUserID) 
    ON UPDATE CASCADE ON DELETE SET NULL,
  CONSTRAINT FKEmployeeRefEmployeeTitle FOREIGN KEY(EmployeeTitleID) REFERENCES EmployeeTitle(EmployeeTitleID) 
    ON UPDATE CASCADE ON DELETE NO ACTION
);

INSERT INTO Employee
  VALUES (1, '000', 0, 1, NULL, '', '', 'Dummy User', '', 'Dummy City', '1990-01-01', 1, '', '', 'Dummy Address', '', 'Dummy City', '', '', '', '2016-01-01', NULL, 1);

CREATE TABLE AppPrivilege
(
  AppPrivilegeID int NOT NULL AUTO_INCREMENT,
  AppPrivilegeTitle varchar(48) NOT NULL,
  AppPrivilegeGroup varchar(32) NOT NULL DEFAULT '',
  AppPrivilegeDesc varchar(92) NOT NULL DEFAULT '',
  CONSTRAINT PKAppPrivilege PRIMARY KEY (AppPrivilegeID),
  CONSTRAINT UQAppPrivilege UNIQUE (AppPrivilegeTitle)
);

INSERT INTO AppPrivilege
 VALUES(1, 'Employee Master', 'Master', '');

CREATE TABLE AppUserPrivilege
(
  AppUserID int NOT NULL,
  AppPrivilegeID int NOT NULL,
  AppAdd tinyint(1) NULL DEFAULT NULL,
  AppEdit tinyint(1) NULL DEFAULT NULL,
  AppDelete tinyint(1) NULL DEFAULT NULL,
  AppView tinyint(1) NULL DEFAULT NULL,
  AppPrint tinyint(1) NULL DEFAULT NULL,
  UpdateTime datetime NOT NULL,
  CONSTRAINT PKAppUserPrivilege PRIMARY KEY (AppUserID, AppPrivilegeID),
  CONSTRAINT FKUserPrivilegeRefAppUser FOREIGN KEY (AppUserID) REFERENCES AppUser(AppUserID)
    ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT FKUserPrivilegeRefAppPrivilege FOREIGN KEY (AppPrivilegeID) REFERENCES AppPrivilege(AppPrivilegeID)
    ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO AppUserPrivilege
  VALUES (1, 1, 1, 1, 1, 1, 1, '2016-01-01 09:30:00');
