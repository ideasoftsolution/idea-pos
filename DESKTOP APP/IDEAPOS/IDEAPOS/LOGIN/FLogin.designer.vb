﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<Global.System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726")> _
Partial Class FLogin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub
    Friend WithEvents txtUsername As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FLogin))
        Me.txtUsername = New System.Windows.Forms.TextBox
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.btnLogin = New System.Windows.Forms.Button
        Me.lblTitle = New System.Windows.Forms.Label
        Me.pbUsername = New System.Windows.Forms.PictureBox
        Me.pbPass = New System.Windows.Forms.PictureBox
        Me.lblVersion = New System.Windows.Forms.Label
        Me.pbTitle = New System.Windows.Forms.PictureBox
        Me.lblAppsName = New System.Windows.Forms.Label
        Me.lblProfile = New System.Windows.Forms.Label
        Me.chkRemember = New System.Windows.Forms.CheckBox
        Me.btnClose = New System.Windows.Forms.Button
        CType(Me.pbUsername, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbPass, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbTitle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtUsername
        '
        Me.txtUsername.BackColor = System.Drawing.Color.White
        Me.txtUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUsername.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtUsername.Font = New System.Drawing.Font("Arial", 17.0!)
        Me.txtUsername.Location = New System.Drawing.Point(79, 105)
        Me.txtUsername.MaxLength = 24
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(298, 34)
        Me.txtUsername.TabIndex = 1
        '
        'txtPassword
        '
        Me.txtPassword.BackColor = System.Drawing.Color.White
        Me.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPassword.Font = New System.Drawing.Font("Arial", 17.0!)
        Me.txtPassword.Location = New System.Drawing.Point(79, 145)
        Me.txtPassword.MaxLength = 64
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(298, 34)
        Me.txtPassword.TabIndex = 2
        Me.txtPassword.UseSystemPasswordChar = True
        '
        'btnLogin
        '
        Me.btnLogin.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLogin.BackColor = System.Drawing.Color.SteelBlue
        Me.btnLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLogin.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnLogin.ForeColor = System.Drawing.Color.White
        Me.btnLogin.Image = Global.IDEAPOS.My.Resources.Resources.user_login_button_24
        Me.btnLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLogin.Location = New System.Drawing.Point(383, 228)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Padding = New System.Windows.Forms.Padding(5)
        Me.btnLogin.Size = New System.Drawing.Size(95, 39)
        Me.btnLogin.TabIndex = 4
        Me.btnLogin.Text = "Log In"
        Me.btnLogin.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLogin.UseVisualStyleBackColor = False
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Arial", 12.0!)
        Me.lblTitle.ForeColor = System.Drawing.Color.White
        Me.lblTitle.Location = New System.Drawing.Point(38, 84)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(59, 18)
        Me.lblTitle.TabIndex = 5
        Me.lblTitle.Text = "LOG IN"
        '
        'pbUsername
        '
        Me.pbUsername.BackColor = System.Drawing.Color.LightSlateGray
        Me.pbUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pbUsername.Image = Global.IDEAPOS.My.Resources.Resources.user_black_close_up_shape_24
        Me.pbUsername.Location = New System.Drawing.Point(41, 105)
        Me.pbUsername.Name = "pbUsername"
        Me.pbUsername.Size = New System.Drawing.Size(39, 34)
        Me.pbUsername.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pbUsername.TabIndex = 6
        Me.pbUsername.TabStop = False
        '
        'pbPass
        '
        Me.pbPass.BackColor = System.Drawing.Color.LightSlateGray
        Me.pbPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pbPass.Image = Global.IDEAPOS.My.Resources.Resources.key_24
        Me.pbPass.Location = New System.Drawing.Point(41, 145)
        Me.pbPass.Name = "pbPass"
        Me.pbPass.Size = New System.Drawing.Size(39, 34)
        Me.pbPass.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pbPass.TabIndex = 7
        Me.pbPass.TabStop = False
        '
        'lblVersion
        '
        Me.lblVersion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblVersion.AutoSize = True
        Me.lblVersion.BackColor = System.Drawing.Color.Transparent
        Me.lblVersion.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblVersion.ForeColor = System.Drawing.Color.Silver
        Me.lblVersion.Location = New System.Drawing.Point(14, 286)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(80, 14)
        Me.lblVersion.TabIndex = 8
        Me.lblVersion.Text = "V: 0000000001"
        '
        'pbTitle
        '
        Me.pbTitle.BackColor = System.Drawing.Color.Transparent
        Me.pbTitle.Image = Global.IDEAPOS.My.Resources.Resources.icon
        Me.pbTitle.Location = New System.Drawing.Point(0, -1)
        Me.pbTitle.Name = "pbTitle"
        Me.pbTitle.Size = New System.Drawing.Size(38, 43)
        Me.pbTitle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbTitle.TabIndex = 9
        Me.pbTitle.TabStop = False
        '
        'lblAppsName
        '
        Me.lblAppsName.BackColor = System.Drawing.Color.Transparent
        Me.lblAppsName.Font = New System.Drawing.Font("Segoe UI Light", 15.75!)
        Me.lblAppsName.ForeColor = System.Drawing.Color.White
        Me.lblAppsName.Location = New System.Drawing.Point(23, -1)
        Me.lblAppsName.Name = "lblAppsName"
        Me.lblAppsName.Size = New System.Drawing.Size(265, 43)
        Me.lblAppsName.TabIndex = 10
        Me.lblAppsName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblProfile
        '
        Me.lblProfile.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblProfile.BackColor = System.Drawing.Color.Transparent
        Me.lblProfile.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblProfile.ForeColor = System.Drawing.Color.Silver
        Me.lblProfile.Location = New System.Drawing.Point(380, 286)
        Me.lblProfile.Name = "lblProfile"
        Me.lblProfile.Size = New System.Drawing.Size(124, 14)
        Me.lblProfile.TabIndex = 11
        Me.lblProfile.Text = "PROFILE:"
        Me.lblProfile.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkRemember
        '
        Me.chkRemember.AutoSize = True
        Me.chkRemember.BackColor = System.Drawing.Color.Transparent
        Me.chkRemember.Location = New System.Drawing.Point(79, 185)
        Me.chkRemember.Name = "chkRemember"
        Me.chkRemember.Size = New System.Drawing.Size(151, 21)
        Me.chkRemember.TabIndex = 3
        Me.chkRemember.Text = "Remember Username"
        Me.chkRemember.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.IndianRed
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(314, 228)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(63, 39)
        Me.btnClose.TabIndex = 13
        Me.btnClose.TabStop = False
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'FLogin
        '
        Me.AcceptButton = Me.btnLogin
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.IDEAPOS.My.Resources.Resources.bg_11
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(516, 309)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.chkRemember)
        Me.Controls.Add(Me.lblProfile)
        Me.Controls.Add(Me.pbTitle)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.pbPass)
        Me.Controls.Add(Me.pbUsername)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.btnLogin)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtUsername)
        Me.Controls.Add(Me.lblAppsName)
        Me.Font = New System.Drawing.Font("Calibri", 10.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FLogin"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "IDEAPOS APPLICATION"
        CType(Me.pbUsername, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbPass, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbTitle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnLogin As System.Windows.Forms.Button
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents pbUsername As System.Windows.Forms.PictureBox
    Friend WithEvents pbPass As System.Windows.Forms.PictureBox
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents pbTitle As System.Windows.Forms.PictureBox
    Friend WithEvents lblAppsName As System.Windows.Forms.Label
    Friend WithEvents lblProfile As System.Windows.Forms.Label
    Friend WithEvents chkRemember As System.Windows.Forms.CheckBox
    Friend WithEvents btnClose As System.Windows.Forms.Button

End Class
