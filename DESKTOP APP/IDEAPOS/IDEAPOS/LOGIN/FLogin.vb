﻿Public Class FLogin
    Private Sub _LoginTemplate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblVersion.Text = getVersion()
        txtUsername.Text = getUID()
        lblProfile.Text = getConn()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        If tryLogin() Then
            'Me.Hide()
            If chkRemember.Checked Then
                setUID(txtUsername.Text.Trim)
            Else
                setUID("")
            End If
        End If
    End Sub

    Private Function tryLogin()
        pastikanKoneksi()
        query = "SELECT * FROM appuser WHERE AppUserName = @AppUserName AND AppUserPassword = MD5(@AppUserPassword) AND Active = 1"
        mycmd = New MySqlCommand(query, mycon)
        mycmd.Parameters.AddWithValue("@AppUserName", txtUsername.Text.Trim)
        mycmd.Parameters.AddWithValue("@AppUserPassword", txtPassword.Text)
        Try
            myda = New MySqlDataAdapter(mycmd)
            If ds.Tables.Contains("AppUser") = True Then
                ds.Tables("AppUser").Rows.Clear()
            End If
            myda.Fill(ds, "AppUser")
        Catch ex As Exception

        End Try
        Return 1
    End Function

#Region "Form Moving"
    'FORM MOVING
    Public MoveForm As Boolean
    Public MoveForm_MousePosition As Point
    Public Sub MoveForm_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _
    MyBase.MouseDown ' Add more handles here (Example: PictureBox1.MouseDown)
        If e.Button = MouseButtons.Left Then
            MoveForm = True
            Me.Cursor = Cursors.NoMove2D
            MoveForm_MousePosition = e.Location
        End If
    End Sub

    Public Sub MoveForm_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _
    MyBase.MouseMove ' Add more handles here (Example: PictureBox1.MouseMove)
        If MoveForm Then
            Me.Location = Me.Location + (e.Location - MoveForm_MousePosition)
        End If
    End Sub

    Public Sub MoveForm_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _
    MyBase.MouseUp ' Add more handles here (Example: PictureBox1.MouseUp)
        If e.Button = MouseButtons.Left Then
            MoveForm = False
            Me.Cursor = Cursors.Default
        End If
    End Sub
#End Region

   
End Class
