﻿Imports System.IO
Module ModulGlobal
    Public appVersion = ""
    Public connDataSource = ""
    Public connDatabase = ""
    Public connUid = ""
    Public connPwd = ""
    Public connProfiles = ""
    Public connString = ""
    Public mycon As New MySqlConnection(connString)
    Public myda As MySqlDataAdapter
    Public ds As New DataSet
    Public mycmd As MySqlCommand
    Public query As String
    Public cw As DataGridViewColumn
    Public Function pastikanKoneksi()
        Try
            If mycon.State = ConnectionState.Closed Then
                mycon.Open()
            End If
        Catch ex As Exception
            'MessageBox.Show("Koneksi Gagal", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
        End Try
        Return True
    End Function

    'AMBIL VERSI APLIKASI DARI FILE
    Public Function getVersion()
        Dim list As New List(Of String)
        Using r As StreamReader = New StreamReader("config/version.myapps")
            Dim line As String
            line = r.ReadLine
            Do While (Not line Is Nothing)
                list.Add(line)
                Dim words As String() = line.Split(New Char() {"="c})
                Select Case words(0)
                    Case "VERSION"
                        appVersion = words(1)
                    Case Else
                End Select
                line = r.ReadLine
            Loop
        End Using
        Return appVersion
    End Function
    'END OF AMBIL VERSI

    'AMBIL/TULIS REMEMBERED USERNAME
    Public Function getUID()
        Dim RememberedUserName As String = ""
        Dim list As New List(Of String)
        Using r As StreamReader = New StreamReader("config/uid.myapps")
            Dim line As String
            line = r.ReadLine
            Do While (Not line Is Nothing)
                list.Add(line)
                Dim words As String() = line.Split(New Char() {"="c})
                Select Case words(0)
                    Case "USERNAME"
                        RememberedUserName = words(1)
                    Case Else
                End Select
                line = r.ReadLine
            Loop
        End Using
        Return RememberedUserName
    End Function

    Public Function setUID(ByVal NewUserName)
        Try
            Using writer As StreamWriter = New StreamWriter("config/uid.myapps")
                writer.Write("USERNAME=" & NewUserName)
            End Using
        Catch ex As Exception
            Return 0
        End Try        
        Return 1
    End Function
    'END OF REMEMBERED USERNAME

    'AMBIL KONEKSI DARI FILE
    Public Function getConn()
        connDataSource = ""
        connDatabase = ""
        connUid = ""
        connPwd = ""
        Dim list As New List(Of String)
        Using r As StreamReader = New StreamReader("config/config.myapps")
            Dim line As String
            line = r.ReadLine
            Do While (Not line Is Nothing)
                list.Add(line)
                Dim words As String() = line.Split(New Char() {"="c})
                Select Case words(0)
                    Case "PROFILE"
                        connProfiles = words(1)
                    Case "SOURCE"
                        connDataSource = words(1)
                    Case "DB"
                        connDatabase = words(1)
                    Case "UID"
                        connUid = words(1)
                    Case "PWD"
                        connPwd = words(1)
                    Case Else
                End Select
                connString = "Data Source=" & connDataSource & ";" & _
                        "Database=" & connDatabase & ";" & _
                        "Uid=" & connUid & ";" & _
                        "Pwd=" & connPwd
                mycon = New MySqlConnection(connString)
                line = r.ReadLine
            Loop
        End Using
        Return connProfiles
    End Function
    'END OF AMBIL KONEKSI

    'TERAPKAN FORM CONFIG
    Public Function applyFVConfig(ByVal frmName As String, ByRef lblTitle As Label)
        Dim list As New List(Of String)
        Using r As StreamReader = New StreamReader("config-form/" & frmName & ".myapps")
            Dim line As String
            line = r.ReadLine
            Do While (Not line Is Nothing)
                list.Add(line)
                Dim words As String() = line.Split(New Char() {"="c})
                Select Case words(0)
                    Case "FORMTITLE"
                        lblTitle.Text = words(1)
                    Case Else
                End Select
                line = r.ReadLine
            Loop
        End Using
        Return 1
    End Function
    Public Function applyDIConfig(ByVal frmName As String, ByRef lblTitle As Label, ByRef lblSubtitle As Label)
        Dim list As New List(Of String)
        Using r As StreamReader = New StreamReader("config-form/" & frmName & ".myapps")
            Dim line As String
            line = r.ReadLine
            Do While (Not line Is Nothing)
                list.Add(line)
                Dim words As String() = line.Split(New Char() {"="c})
                Select Case words(0)
                    Case "FORMTITLE"
                        lblTitle.Text = words(1)
                    Case "FORMSUBTITLE"
                        lblSubtitle.Text = words(1)
                    Case Else
                End Select
                line = r.ReadLine
            Loop
        End Using
        Return 1
    End Function
    'END OF TERAPKAN FORM CONFIG

    'TERAPKAN KONFIGURASI KOLOM DAN DATA DGV
    Public Function applyDGV(ByVal frmName As String, ByRef dgv As DataGridView)
        dgv.Columns.Clear()
        Dim list As New List(Of String)
        Using r As StreamReader = New StreamReader("config-dgv-data/dgv-" & frmName & ".myapps")
            Dim line As String
            line = r.ReadLine
            Do While (Not line Is Nothing)
                list.Add(line)
                Dim words As String() = line.Split(New Char() {";"c})
                Select Case words(3)
                    Case "TextBox"
                        Dim col As New DataGridViewTextBoxColumn
                        col.Name = words(0)
                        col.HeaderText = words(1)
                        col.Width = words(2)
                        col.DefaultCellStyle.Format = words(4)
                        col.DefaultCellStyle.Alignment = IIf(words(5) = "L", DataGridViewContentAlignment.MiddleLeft, IIf(words(5) = "M", DataGridViewContentAlignment.MiddleCenter, DataGridViewContentAlignment.MiddleRight))
                        col.Visible = words(6)
                        col.SortMode = IIf(words(7), DataGridViewColumnSortMode.Automatic, DataGridViewColumnSortMode.NotSortable)
                        dgv.Columns.Add(col)
                    Case "CheckBox"
                        Dim col As New DataGridViewCheckBoxColumn
                        col.Name = words(0)
                        col.HeaderText = words(1)
                        col.Width = words(2)
                        col.DefaultCellStyle.Alignment = IIf(words(5) = "L", DataGridViewContentAlignment.MiddleLeft, IIf(words(5) = "M", DataGridViewContentAlignment.MiddleCenter, DataGridViewContentAlignment.MiddleRight))
                        col.Visible = words(6)
                        col.SortMode = IIf(words(7), DataGridViewColumnSortMode.Automatic, DataGridViewColumnSortMode.NotSortable)
                        dgv.Columns.Add(col)
                    Case "Link"
                        Dim col As New DataGridViewLinkColumn
                        col.Name = words(0)
                        col.HeaderText = words(1)
                        col.Width = words(2)
                        col.DefaultCellStyle.Alignment = IIf(words(5) = "L", DataGridViewContentAlignment.MiddleLeft, IIf(words(5) = "M", DataGridViewContentAlignment.MiddleCenter, DataGridViewContentAlignment.MiddleRight))
                        col.Visible = words(6)
                        col.SortMode = IIf(words(7), DataGridViewColumnSortMode.Automatic, DataGridViewColumnSortMode.NotSortable)
                        dgv.Columns.Add(col)
                    Case "Button"
                        Dim col As New DataGridViewButtonColumn
                        col.Name = words(0)
                        col.HeaderText = words(1)
                        col.Width = words(2)
                        col.DefaultCellStyle.Alignment = IIf(words(5) = "L", DataGridViewContentAlignment.MiddleLeft, IIf(words(5) = "M", DataGridViewContentAlignment.MiddleCenter, DataGridViewContentAlignment.MiddleRight))
                        col.Visible = words(6)
                        col.SortMode = IIf(words(7), DataGridViewColumnSortMode.Automatic, DataGridViewColumnSortMode.NotSortable)
                        dgv.Columns.Add(col)
                    Case Else
                        Dim col As New DataGridViewTextBoxColumn
                        col.Name = words(0)
                        col.HeaderText = words(1)
                        col.Width = words(2)
                        col.DefaultCellStyle.Format = words(4)
                        col.DefaultCellStyle.Alignment = IIf(words(5) = "L", DataGridViewContentAlignment.MiddleLeft, IIf(words(5) = "M", DataGridViewContentAlignment.MiddleCenter, DataGridViewContentAlignment.MiddleRight))
                        col.Visible = words(6)
                        col.SortMode = IIf(words(7), DataGridViewColumnSortMode.Automatic, DataGridViewColumnSortMode.NotSortable)
                        dgv.Columns.Add(col)
                End Select
                line = r.ReadLine
            Loop
        End Using
        Return 1
    End Function
    Public Function applyDGVData(ByVal frmName As String, ByRef dgv As DataGridView, Optional ByVal defaultFilterName As String = "", Optional ByVal defaultFilterValue As String = "")
        dgv.Rows.Clear()
        Dim list As New List(Of String)
        Dim dt As String = ""
        Dim defaultFilter As String = IIf(defaultFilterName.ToString.Trim = "", "", defaultFilterName & " like '%" & defaultFilterValue & "%'")
        Using r As StreamReader = New StreamReader("config-dgv-data/data-" & frmName & ".myapps")
            Dim line As String
            line = r.ReadLine
            Do While (Not line Is Nothing)
                list.Add(line)
                Dim words As String() = line.Split(New Char() {"="c})
                Select Case words(0)
                    Case "DT"
                        dt = words(1)
                    Case Else
                End Select
                line = r.ReadLine
            Loop
        End Using
        Dim result() As DataRow = ds.Tables(dt).Select(defaultFilter)
        For Each row As DataRow In result
            Dim n As Integer = dgv.Rows.Add()
            For i As Integer = 0 To dgv.Columns.Count - 1
                dgv.Rows.Item(n).Cells(dgv.Columns(i).Name.ToString).Value = row(dgv.Columns(i).Name.ToString)
            Next
        Next
        Return 1
    End Function
    'END OF KONFIGURASI DGV

    Public Function namaBulan(ByVal bulan As Integer)
        Dim nama As String() = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", _
                                "Juli", "Agustus", "September", "Oktober", "November", "Desember"}
        Return nama(bulan)
    End Function
    Public Function namaBulanSingkat(ByVal bulan As Integer)
        Dim nama As String() = {"Jan", "Feb", "Mar", "Apr", "Mei", "Jun", _
                                "Jul", "Ags", "Sep", "Okt", "Nov", "Des"}
        Return nama(bulan)
    End Function
    Public Sub setLabelTitle(ByRef lblTitle As Label, ByRef modeInsert As Boolean)
        If modeInsert Then
            lblTitle.Text = "[ENTRY DATA BARU]"
        Else
            lblTitle.Text = "[EDIT / UBAH DATA]"
        End If
    End Sub

    'Default DataSet Global & Parameter Global
    Public Sub tryClearDataTable(ByVal dtName)
        Try
            If ds.Tables.Contains(dtName) = True Then
                ds.Tables(dtName).Rows.Clear()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub isiCboBulan(ByRef cbo As ComboBox)
        Dim dt = "sekarang"
        Try
            cbo.Items.Clear()
            For i As Integer = 0 To 11
                cbo.Items.Add(namaBulan(i).ToString.ToUpper)
            Next
            cbo.SelectedIndex = ds.Tables(dt).Rows(0)("bulan") - 1
        Catch ex As Exception

        End Try
    End Sub

    Public Sub isiCboTahun(ByRef cbo As ComboBox)
        Dim dt = "sekarang"
        Try
            cbo.Items.Clear()
            For i As Integer = -1 To 1
                cbo.Items.Add(ds.Tables("sekarang").Rows(0)("tahun") + i)
            Next
            cbo.SelectedIndex = 1
        Catch ex As Exception

        End Try
    End Sub

    Public Sub isiDsBulanTahun()
        Dim dt = "sekarang"
        pastikanKoneksi()
        query = "select month(now()) bulan, year(now()) tahun "
        Try
            mycmd = New MySqlCommand(query, mycon)
            tryClearDataTable(dt)
            myda = New MySqlDataAdapter(mycmd)
            myda.Fill(ds, dt)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub isiDsEmployee()
        pastikanKoneksi()
        Dim list As New List(Of String)
        Dim dt = "employee"
        Dim query As String = ""
        Using r As StreamReader = New StreamReader("config-ds/query-" & dt & ".myapps")
            query = r.ReadToEnd
        End Using
        Try
            mycmd = New MySqlCommand(query, mycon)
            tryClearDataTable(dt)
            myda = New MySqlDataAdapter(mycmd)
            myda.Fill(ds, dt)
            'MsgBox(ds.Tables(dt).Rows.Count)
        Catch ex As Exception
            'MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub resetForm(ByRef ctrl As Control)
        For Each i As Control In ctrl.Controls
            'MsgBox(i.Name)
            If i.GetType() Is GetType(ComboBox) Then
                Dim j As ComboBox = CType(i, ComboBox)
                Try
                    j.SelectedIndex = 0
                Catch ex As Exception

                End Try
            End If
            If i.GetType() Is GetType(TextBox) Then
                i.Text = ""
            End If
            If i.GetType() Is GetType(NumericUpDown) Then
                Dim j As NumericUpDown = CType(i, NumericUpDown)
                j.Value = 0
            End If
            If i.GetType() Is GetType(DateTimePicker) Then
                Dim j As DateTimePicker = CType(i, DateTimePicker)
                j.Value = Now
            End If
        Next
    End Sub
End Module
