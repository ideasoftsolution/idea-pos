﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class _InputFormTemplate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.lblTitle = New System.Windows.Forms.Label
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.lblSubTitle = New System.Windows.Forms.Label
        Me.frmGroup1 = New System.Windows.Forms.Label
        Me.lblGroup2 = New System.Windows.Forms.Label
        Me.txtInput1 = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.dtpInput1 = New System.Windows.Forms.DateTimePicker
        Me.Label2 = New System.Windows.Forms.Label
        Me.nudInput1 = New System.Windows.Forms.NumericUpDown
        Me.dtpInput2 = New System.Windows.Forms.DateTimePicker
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.tanggal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.kode_maintenance = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.uraian_pekerjaan = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.posisi_kapal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.sparepart_yang_dipakai = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.jumlah = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.sisa_stok = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.hour_meter = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.keterangan = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnOpen = New System.Windows.Forms.Button
        Me.btnSubmit = New System.Windows.Forms.Button
        Me.btnNew = New System.Windows.Forms.Button
        Me.btnMaximize = New System.Windows.Forms.Button
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.pnlMain.SuspendLayout()
        CType(Me.nudInput1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.MidnightBlue
        Me.lblTitle.Font = New System.Drawing.Font("Arial", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitle.ForeColor = System.Drawing.Color.White
        Me.lblTitle.Location = New System.Drawing.Point(-1, 0)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Padding = New System.Windows.Forms.Padding(12, 0, 0, 0)
        Me.lblTitle.Size = New System.Drawing.Size(248, 36)
        Me.lblTitle.TabIndex = 2
        Me.lblTitle.Text = "FORM TITLE"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlMain
        '
        Me.pnlMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlMain.BackColor = System.Drawing.Color.White
        Me.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMain.Controls.Add(Me.dgv)
        Me.pnlMain.Controls.Add(Me.btnBrowse)
        Me.pnlMain.Controls.Add(Me.dtpInput2)
        Me.pnlMain.Controls.Add(Me.nudInput1)
        Me.pnlMain.Controls.Add(Me.Label2)
        Me.pnlMain.Controls.Add(Me.dtpInput1)
        Me.pnlMain.Controls.Add(Me.Label1)
        Me.pnlMain.Controls.Add(Me.txtInput1)
        Me.pnlMain.Controls.Add(Me.lblGroup2)
        Me.pnlMain.Controls.Add(Me.frmGroup1)
        Me.pnlMain.Location = New System.Drawing.Point(12, 136)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(1035, 493)
        Me.pnlMain.TabIndex = 3
        '
        'lblSubTitle
        '
        Me.lblSubTitle.BackColor = System.Drawing.Color.SlateGray
        Me.lblSubTitle.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblSubTitle.ForeColor = System.Drawing.Color.White
        Me.lblSubTitle.Location = New System.Drawing.Point(-1, 34)
        Me.lblSubTitle.Name = "lblSubTitle"
        Me.lblSubTitle.Padding = New System.Windows.Forms.Padding(13, 0, 0, 0)
        Me.lblSubTitle.Size = New System.Drawing.Size(248, 22)
        Me.lblSubTitle.TabIndex = 4
        Me.lblSubTitle.Text = "FORM SUBTITLE"
        Me.lblSubTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmGroup1
        '
        Me.frmGroup1.BackColor = System.Drawing.Color.LightGray
        Me.frmGroup1.Location = New System.Drawing.Point(0, 14)
        Me.frmGroup1.Name = "frmGroup1"
        Me.frmGroup1.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.frmGroup1.Size = New System.Drawing.Size(234, 26)
        Me.frmGroup1.TabIndex = 0
        Me.frmGroup1.Text = "GROUP 1"
        Me.frmGroup1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGroup2
        '
        Me.lblGroup2.BackColor = System.Drawing.Color.LightGray
        Me.lblGroup2.Location = New System.Drawing.Point(466, 14)
        Me.lblGroup2.Name = "lblGroup2"
        Me.lblGroup2.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.lblGroup2.Size = New System.Drawing.Size(234, 26)
        Me.lblGroup2.TabIndex = 1
        Me.lblGroup2.Text = "GROUP 2"
        Me.lblGroup2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInput1
        '
        Me.txtInput1.Location = New System.Drawing.Point(70, 58)
        Me.txtInput1.Name = "txtInput1"
        Me.txtInput1.Size = New System.Drawing.Size(279, 23)
        Me.txtInput1.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 61)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Label1"
        '
        'dtpInput1
        '
        Me.dtpInput1.CustomFormat = "dd.MM.yyyy"
        Me.dtpInput1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpInput1.Location = New System.Drawing.Point(70, 87)
        Me.dtpInput1.Name = "dtpInput1"
        Me.dtpInput1.Size = New System.Drawing.Size(116, 23)
        Me.dtpInput1.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 92)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 16)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Label2"
        '
        'nudInput1
        '
        Me.nudInput1.Location = New System.Drawing.Point(70, 116)
        Me.nudInput1.Name = "nudInput1"
        Me.nudInput1.Size = New System.Drawing.Size(116, 23)
        Me.nudInput1.TabIndex = 6
        '
        'dtpInput2
        '
        Me.dtpInput2.CustomFormat = "HH:mm:ss"
        Me.dtpInput2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpInput2.Location = New System.Drawing.Point(192, 87)
        Me.dtpInput2.Name = "dtpInput2"
        Me.dtpInput2.ShowUpDown = True
        Me.dtpInput2.Size = New System.Drawing.Size(81, 23)
        Me.dtpInput2.TabIndex = 7
        '
        'dgv
        '
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv.BackgroundColor = System.Drawing.Color.White
        Me.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.Yellow
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Arial", 10.0!)
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.tanggal, Me.kode_maintenance, Me.uraian_pekerjaan, Me.posisi_kapal, Me.sparepart_yang_dipakai, Me.jumlah, Me.sisa_stok, Me.hour_meter, Me.keterangan, Me.id})
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Arial", 10.0!)
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv.DefaultCellStyle = DataGridViewCellStyle14
        Me.dgv.EnableHeadersVisualStyles = False
        Me.dgv.GridColor = System.Drawing.Color.Silver
        Me.dgv.Location = New System.Drawing.Point(3, 183)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowHeadersWidth = 40
        Me.dgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgv.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Calibri", 10.0!)
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.Size = New System.Drawing.Size(1027, 305)
        Me.dgv.TabIndex = 17
        '
        'tanggal
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.tanggal.DefaultCellStyle = DataGridViewCellStyle9
        Me.tanggal.HeaderText = "Tanggal"
        Me.tanggal.Name = "tanggal"
        Me.tanggal.ReadOnly = True
        '
        'kode_maintenance
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.kode_maintenance.DefaultCellStyle = DataGridViewCellStyle10
        Me.kode_maintenance.HeaderText = "Kode Maintenance"
        Me.kode_maintenance.Name = "kode_maintenance"
        Me.kode_maintenance.ReadOnly = True
        Me.kode_maintenance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'uraian_pekerjaan
        '
        Me.uraian_pekerjaan.HeaderText = "Uraian Pekerjaan"
        Me.uraian_pekerjaan.Name = "uraian_pekerjaan"
        Me.uraian_pekerjaan.ReadOnly = True
        Me.uraian_pekerjaan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.uraian_pekerjaan.Width = 400
        '
        'posisi_kapal
        '
        Me.posisi_kapal.HeaderText = "Posisi Kapal"
        Me.posisi_kapal.Name = "posisi_kapal"
        Me.posisi_kapal.ReadOnly = True
        Me.posisi_kapal.Width = 150
        '
        'sparepart_yang_dipakai
        '
        Me.sparepart_yang_dipakai.HeaderText = "Sparepart yang Dipakai"
        Me.sparepart_yang_dipakai.Name = "sparepart_yang_dipakai"
        Me.sparepart_yang_dipakai.ReadOnly = True
        Me.sparepart_yang_dipakai.Width = 300
        '
        'jumlah
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.jumlah.DefaultCellStyle = DataGridViewCellStyle11
        Me.jumlah.HeaderText = "Jumlah"
        Me.jumlah.Name = "jumlah"
        Me.jumlah.ReadOnly = True
        '
        'sisa_stok
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.sisa_stok.DefaultCellStyle = DataGridViewCellStyle12
        Me.sisa_stok.HeaderText = "Sisa Stok"
        Me.sisa_stok.Name = "sisa_stok"
        Me.sisa_stok.ReadOnly = True
        '
        'hour_meter
        '
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.hour_meter.DefaultCellStyle = DataGridViewCellStyle13
        Me.hour_meter.HeaderText = "Hour Meter"
        Me.hour_meter.Name = "hour_meter"
        Me.hour_meter.ReadOnly = True
        Me.hour_meter.Width = 80
        '
        'keterangan
        '
        Me.keterangan.HeaderText = "Keterangan"
        Me.keterangan.Name = "keterangan"
        Me.keterangan.ReadOnly = True
        Me.keterangan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.keterangan.Width = 600
        '
        'id
        '
        Me.id.HeaderText = "id"
        Me.id.Name = "id"
        Me.id.ReadOnly = True
        Me.id.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.IndianRed
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Image = Global.IDEAPOS.My.Resources.Resources.close_button_16
        Me.btnClose.Location = New System.Drawing.Point(1025, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(34, 32)
        Me.btnClose.TabIndex = 14
        Me.btnClose.TabStop = False
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.IndianRed
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnDelete.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.btnDelete.ForeColor = System.Drawing.Color.White
        Me.btnDelete.Image = Global.IDEAPOS.My.Resources.Resources.rubbish_bin_32
        Me.btnDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDelete.Location = New System.Drawing.Point(155, 74)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(73, 63)
        Me.btnDelete.TabIndex = 21
        Me.btnDelete.TabStop = False
        Me.btnDelete.Text = "DELETE"
        Me.btnDelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'btnOpen
        '
        Me.btnOpen.BackColor = System.Drawing.Color.Goldenrod
        Me.btnOpen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpen.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnOpen.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpen.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.btnOpen.ForeColor = System.Drawing.Color.White
        Me.btnOpen.Image = Global.IDEAPOS.My.Resources.Resources.open_folder_32
        Me.btnOpen.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnOpen.Location = New System.Drawing.Point(83, 74)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.Size = New System.Drawing.Size(73, 63)
        Me.btnOpen.TabIndex = 20
        Me.btnOpen.TabStop = False
        Me.btnOpen.Text = "EDIT"
        Me.btnOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnOpen.UseVisualStyleBackColor = False
        '
        'btnSubmit
        '
        Me.btnSubmit.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.btnSubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSubmit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSubmit.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubmit.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.btnSubmit.ForeColor = System.Drawing.Color.White
        Me.btnSubmit.Image = Global.IDEAPOS.My.Resources.Resources.save_icon_32
        Me.btnSubmit.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSubmit.Location = New System.Drawing.Point(227, 74)
        Me.btnSubmit.Name = "btnSubmit"
        Me.btnSubmit.Size = New System.Drawing.Size(73, 63)
        Me.btnSubmit.TabIndex = 19
        Me.btnSubmit.TabStop = False
        Me.btnSubmit.Text = "SUBMIT"
        Me.btnSubmit.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSubmit.UseVisualStyleBackColor = False
        '
        'btnNew
        '
        Me.btnNew.BackColor = System.Drawing.Color.LightSlateGray
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnNew.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.btnNew.ForeColor = System.Drawing.Color.White
        Me.btnNew.Image = Global.IDEAPOS.My.Resources.Resources.edit_32
        Me.btnNew.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnNew.Location = New System.Drawing.Point(12, 74)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(73, 63)
        Me.btnNew.TabIndex = 18
        Me.btnNew.TabStop = False
        Me.btnNew.Text = "NEW"
        Me.btnNew.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnNew.UseVisualStyleBackColor = False
        '
        'btnMaximize
        '
        Me.btnMaximize.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMaximize.BackColor = System.Drawing.Color.DarkGray
        Me.btnMaximize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnMaximize.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnMaximize.FlatAppearance.BorderSize = 0
        Me.btnMaximize.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMaximize.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnMaximize.ForeColor = System.Drawing.Color.White
        Me.btnMaximize.Image = Global.IDEAPOS.My.Resources.Resources.expand_button_16
        Me.btnMaximize.Location = New System.Drawing.Point(991, 0)
        Me.btnMaximize.Name = "btnMaximize"
        Me.btnMaximize.Size = New System.Drawing.Size(34, 32)
        Me.btnMaximize.TabIndex = 15
        Me.btnMaximize.TabStop = False
        Me.btnMaximize.UseVisualStyleBackColor = False
        '
        'btnBrowse
        '
        Me.btnBrowse.BackColor = System.Drawing.Color.Gray
        Me.btnBrowse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnBrowse.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnBrowse.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBrowse.Font = New System.Drawing.Font("Arial", 10.0!)
        Me.btnBrowse.ForeColor = System.Drawing.Color.White
        Me.btnBrowse.Image = Global.IDEAPOS.My.Resources.Resources.open_folder_16
        Me.btnBrowse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBrowse.Location = New System.Drawing.Point(355, 52)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(85, 29)
        Me.btnBrowse.TabIndex = 16
        Me.btnBrowse.TabStop = False
        Me.btnBrowse.Text = "Browse"
        Me.btnBrowse.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnBrowse.UseVisualStyleBackColor = False
        '
        '_InputFormTemplate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Silver
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(1059, 641)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnOpen)
        Me.Controls.Add(Me.btnSubmit)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.btnMaximize)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.lblSubTitle)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Arial", 10.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "_InputFormTemplate"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "_InputFormTemplate"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        CType(Me.nudInput1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents lblSubTitle As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnMaximize As System.Windows.Forms.Button
    Friend WithEvents frmGroup1 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtInput1 As System.Windows.Forms.TextBox
    Friend WithEvents lblGroup2 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtpInput1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents nudInput1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents dtpInput2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents tanggal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents kode_maintenance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents uraian_pekerjaan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents posisi_kapal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sparepart_yang_dipakai As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents jumlah As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sisa_stok As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hour_meter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents keterangan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents btnSubmit As System.Windows.Forms.Button
    Friend WithEvents btnOpen As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button

End Class
