﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class _MDITemplate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(_MDITemplate))
        Me.StatusStrip = New System.Windows.Forms.StatusStrip
        Me.LoggedAs = New System.Windows.Forms.ToolStripStatusLabel
        Me.DtTm = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.MainMenu = New System.Windows.Forms.MenuStrip
        Me.ConfigurationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.lblAppsName = New System.Windows.Forms.Label
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.StatusStrip.SuspendLayout()
        Me.MainMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStrip
        '
        Me.StatusStrip.BackColor = System.Drawing.Color.White
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LoggedAs, Me.DtTm})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 391)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Padding = New System.Windows.Forms.Padding(2, 0, 21, 0)
        Me.StatusStrip.Size = New System.Drawing.Size(815, 22)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'LoggedAs
        '
        Me.LoggedAs.ForeColor = System.Drawing.Color.Gray
        Me.LoggedAs.Name = "LoggedAs"
        Me.LoggedAs.Size = New System.Drawing.Size(66, 17)
        Me.LoggedAs.Text = "Logged As:"
        '
        'DtTm
        '
        Me.DtTm.ForeColor = System.Drawing.Color.Gray
        Me.DtTm.Name = "DtTm"
        Me.DtTm.Size = New System.Drawing.Size(158, 17)
        Me.DtTm.Text = "Minggu, 00-00-0000 00:00:00"
        '
        'MainMenu
        '
        Me.MainMenu.AutoSize = False
        Me.MainMenu.BackColor = System.Drawing.Color.White
        Me.MainMenu.Font = New System.Drawing.Font("Arial", 11.0!)
        Me.MainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConfigurationToolStripMenuItem})
        Me.MainMenu.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu.Name = "MainMenu"
        Me.MainMenu.Size = New System.Drawing.Size(815, 29)
        Me.MainMenu.TabIndex = 9
        Me.MainMenu.Text = "MainMenu"
        '
        'ConfigurationToolStripMenuItem
        '
        Me.ConfigurationToolStripMenuItem.ForeColor = System.Drawing.Color.Black
        Me.ConfigurationToolStripMenuItem.Name = "ConfigurationToolStripMenuItem"
        Me.ConfigurationToolStripMenuItem.Size = New System.Drawing.Size(62, 25)
        Me.ConfigurationToolStripMenuItem.Text = "Config"
        '
        'lblAppsName
        '
        Me.lblAppsName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblAppsName.BackColor = System.Drawing.Color.White
        Me.lblAppsName.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblAppsName.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblAppsName.Location = New System.Drawing.Point(628, 0)
        Me.lblAppsName.Name = "lblAppsName"
        Me.lblAppsName.Size = New System.Drawing.Size(187, 29)
        Me.lblAppsName.TabIndex = 11
        Me.lblAppsName.Text = "IDEAPOS APPLICATION"
        Me.lblAppsName.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(815, 24)
        Me.MenuStrip1.TabIndex = 12
        Me.MenuStrip1.Text = "MenuStrip1"
        Me.MenuStrip1.Visible = False
        '
        '_MDITemplate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.IDEAPOS.My.Resources.Resources.icon
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(815, 413)
        Me.Controls.Add(Me.lblAppsName)
        Me.Controls.Add(Me.StatusStrip)
        Me.Controls.Add(Me.MainMenu)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Arial", 12.0!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "_MDITemplate"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "_MDITemplate"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.MainMenu.ResumeLayout(False)
        Me.MainMenu.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents MainMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents ConfigurationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblAppsName As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents LoggedAs As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents DtTm As System.Windows.Forms.ToolStripStatusLabel

End Class
