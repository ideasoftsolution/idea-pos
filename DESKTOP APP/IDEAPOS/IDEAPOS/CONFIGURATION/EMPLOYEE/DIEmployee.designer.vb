﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DIEmployee
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTitle = New System.Windows.Forms.Label
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.lblAppUserNameInfo = New System.Windows.Forms.Label
        Me.chkAppUserActive = New System.Windows.Forms.CheckBox
        Me.txtAppUserDesc = New System.Windows.Forms.TextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.txtAppUserName = New System.Windows.Forms.TextBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.pnlSignOut = New System.Windows.Forms.Panel
        Me.Label18 = New System.Windows.Forms.Label
        Me.dtpSignOutDate = New System.Windows.Forms.DateTimePicker
        Me.chkSetSignOut = New System.Windows.Forms.CheckBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.dtpSignInDate = New System.Windows.Forms.DateTimePicker
        Me.txtCountry = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.txtProvince = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtPostalCode = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtCity = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtAddressLine2 = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtAddressLine1 = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtEmail = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtPhone = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.pnlGender = New System.Windows.Forms.Panel
        Me.rdoWoman = New System.Windows.Forms.RadioButton
        Me.rdoMan = New System.Windows.Forms.RadioButton
        Me.Label8 = New System.Windows.Forms.Label
        Me.dtpBirthDate = New System.Windows.Forms.DateTimePicker
        Me.txtBirthPlace = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtIDCat = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtIDNum = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtNickName = New System.Windows.Forms.TextBox
        Me.cboEmployeeTitleName = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.chkAutoEmployeeID = New System.Windows.Forms.CheckBox
        Me.txtEmployeeCompanyID = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtFullName = New System.Windows.Forms.TextBox
        Me.lblGroup2 = New System.Windows.Forms.Label
        Me.frmGroup1 = New System.Windows.Forms.Label
        Me.lblSubTitle = New System.Windows.Forms.Label
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnSubmit = New System.Windows.Forms.Button
        Me.btnNew = New System.Windows.Forms.Button
        Me.pnlMain.SuspendLayout()
        Me.pnlSignOut.SuspendLayout()
        Me.pnlGender.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.MidnightBlue
        Me.lblTitle.Font = New System.Drawing.Font("Arial", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitle.ForeColor = System.Drawing.Color.White
        Me.lblTitle.Location = New System.Drawing.Point(-1, 0)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Padding = New System.Windows.Forms.Padding(12, 0, 0, 0)
        Me.lblTitle.Size = New System.Drawing.Size(248, 36)
        Me.lblTitle.TabIndex = 2
        Me.lblTitle.Text = "FORM TITLE"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlMain
        '
        Me.pnlMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlMain.BackColor = System.Drawing.Color.White
        Me.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMain.Controls.Add(Me.lblAppUserNameInfo)
        Me.pnlMain.Controls.Add(Me.chkAppUserActive)
        Me.pnlMain.Controls.Add(Me.txtAppUserDesc)
        Me.pnlMain.Controls.Add(Me.Label21)
        Me.pnlMain.Controls.Add(Me.txtAppUserName)
        Me.pnlMain.Controls.Add(Me.Label20)
        Me.pnlMain.Controls.Add(Me.Label19)
        Me.pnlMain.Controls.Add(Me.pnlSignOut)
        Me.pnlMain.Controls.Add(Me.chkSetSignOut)
        Me.pnlMain.Controls.Add(Me.Label17)
        Me.pnlMain.Controls.Add(Me.dtpSignInDate)
        Me.pnlMain.Controls.Add(Me.txtCountry)
        Me.pnlMain.Controls.Add(Me.Label16)
        Me.pnlMain.Controls.Add(Me.txtProvince)
        Me.pnlMain.Controls.Add(Me.Label15)
        Me.pnlMain.Controls.Add(Me.txtPostalCode)
        Me.pnlMain.Controls.Add(Me.Label14)
        Me.pnlMain.Controls.Add(Me.txtCity)
        Me.pnlMain.Controls.Add(Me.Label13)
        Me.pnlMain.Controls.Add(Me.txtAddressLine2)
        Me.pnlMain.Controls.Add(Me.Label12)
        Me.pnlMain.Controls.Add(Me.txtAddressLine1)
        Me.pnlMain.Controls.Add(Me.Label11)
        Me.pnlMain.Controls.Add(Me.txtEmail)
        Me.pnlMain.Controls.Add(Me.Label10)
        Me.pnlMain.Controls.Add(Me.txtPhone)
        Me.pnlMain.Controls.Add(Me.Label9)
        Me.pnlMain.Controls.Add(Me.pnlGender)
        Me.pnlMain.Controls.Add(Me.dtpBirthDate)
        Me.pnlMain.Controls.Add(Me.txtBirthPlace)
        Me.pnlMain.Controls.Add(Me.Label7)
        Me.pnlMain.Controls.Add(Me.Label6)
        Me.pnlMain.Controls.Add(Me.txtIDCat)
        Me.pnlMain.Controls.Add(Me.Label5)
        Me.pnlMain.Controls.Add(Me.txtIDNum)
        Me.pnlMain.Controls.Add(Me.Label4)
        Me.pnlMain.Controls.Add(Me.txtNickName)
        Me.pnlMain.Controls.Add(Me.cboEmployeeTitleName)
        Me.pnlMain.Controls.Add(Me.Label3)
        Me.pnlMain.Controls.Add(Me.chkAutoEmployeeID)
        Me.pnlMain.Controls.Add(Me.txtEmployeeCompanyID)
        Me.pnlMain.Controls.Add(Me.Label2)
        Me.pnlMain.Controls.Add(Me.Label1)
        Me.pnlMain.Controls.Add(Me.txtFullName)
        Me.pnlMain.Controls.Add(Me.lblGroup2)
        Me.pnlMain.Controls.Add(Me.frmGroup1)
        Me.pnlMain.Location = New System.Drawing.Point(12, 136)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(1231, 511)
        Me.pnlMain.TabIndex = 3
        '
        'lblAppUserNameInfo
        '
        Me.lblAppUserNameInfo.AutoSize = True
        Me.lblAppUserNameInfo.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblAppUserNameInfo.ForeColor = System.Drawing.Color.Maroon
        Me.lblAppUserNameInfo.Location = New System.Drawing.Point(1116, 46)
        Me.lblAppUserNameInfo.Name = "lblAppUserNameInfo"
        Me.lblAppUserNameInfo.Size = New System.Drawing.Size(80, 14)
        Me.lblAppUserNameInfo.TabIndex = 55
        Me.lblAppUserNameInfo.Text = "Not Available!"
        Me.lblAppUserNameInfo.Visible = False
        '
        'chkAppUserActive
        '
        Me.chkAppUserActive.AutoSize = True
        Me.chkAppUserActive.Checked = True
        Me.chkAppUserActive.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAppUserActive.Location = New System.Drawing.Point(946, 127)
        Me.chkAppUserActive.Name = "chkAppUserActive"
        Me.chkAppUserActive.Size = New System.Drawing.Size(89, 20)
        Me.chkAppUserActive.TabIndex = 25
        Me.chkAppUserActive.Text = "Set Active"
        Me.chkAppUserActive.UseVisualStyleBackColor = True
        Me.chkAppUserActive.Visible = False
        '
        'txtAppUserDesc
        '
        Me.txtAppUserDesc.Location = New System.Drawing.Point(946, 98)
        Me.txtAppUserDesc.Name = "txtAppUserDesc"
        Me.txtAppUserDesc.Size = New System.Drawing.Size(270, 23)
        Me.txtAppUserDesc.TabIndex = 24
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.ForeColor = System.Drawing.Color.Black
        Me.Label21.Location = New System.Drawing.Point(861, 101)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(79, 16)
        Me.Label21.TabIndex = 54
        Me.Label21.Text = "Description"
        '
        'txtAppUserName
        '
        Me.txtAppUserName.Location = New System.Drawing.Point(946, 43)
        Me.txtAppUserName.Name = "txtAppUserName"
        Me.txtAppUserName.Size = New System.Drawing.Size(164, 23)
        Me.txtAppUserName.TabIndex = 23
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.ForeColor = System.Drawing.Color.Black
        Me.Label20.Location = New System.Drawing.Point(863, 46)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(77, 16)
        Me.Label20.TabIndex = 52
        Me.Label20.Text = "User Name"
        '
        'Label19
        '
        Me.Label19.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label19.BackColor = System.Drawing.Color.LightGray
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(852, 9)
        Me.Label19.Name = "Label19"
        Me.Label19.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.Label19.Size = New System.Drawing.Size(378, 26)
        Me.Label19.TabIndex = 51
        Me.Label19.Text = "User Account"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSignOut
        '
        Me.pnlSignOut.Controls.Add(Me.Label18)
        Me.pnlSignOut.Controls.Add(Me.dtpSignOutDate)
        Me.pnlSignOut.Location = New System.Drawing.Point(75, 262)
        Me.pnlSignOut.Name = "pnlSignOut"
        Me.pnlSignOut.Size = New System.Drawing.Size(314, 29)
        Me.pnlSignOut.TabIndex = 8
        Me.pnlSignOut.Visible = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.Color.Black
        Me.Label18.Location = New System.Drawing.Point(45, 8)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(63, 16)
        Me.Label18.TabIndex = 52
        Me.Label18.Text = "Sign Out"
        '
        'dtpSignOutDate
        '
        Me.dtpSignOutDate.CustomFormat = "dd.MM.yyyy"
        Me.dtpSignOutDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpSignOutDate.Location = New System.Drawing.Point(114, 3)
        Me.dtpSignOutDate.Name = "dtpSignOutDate"
        Me.dtpSignOutDate.Size = New System.Drawing.Size(116, 23)
        Me.dtpSignOutDate.TabIndex = 8
        '
        'chkSetSignOut
        '
        Me.chkSetSignOut.AutoSize = True
        Me.chkSetSignOut.Location = New System.Drawing.Point(124, 239)
        Me.chkSetSignOut.Name = "chkSetSignOut"
        Me.chkSetSignOut.Size = New System.Drawing.Size(107, 20)
        Me.chkSetSignOut.TabIndex = 7
        Me.chkSetSignOut.Text = "Set Sign Out"
        Me.chkSetSignOut.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(67, 209)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(51, 16)
        Me.Label17.TabIndex = 49
        Me.Label17.Text = "Sign In"
        '
        'dtpSignInDate
        '
        Me.dtpSignInDate.CustomFormat = "dd.MM.yyyy"
        Me.dtpSignInDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpSignInDate.Location = New System.Drawing.Point(124, 204)
        Me.dtpSignInDate.Name = "dtpSignInDate"
        Me.dtpSignInDate.Size = New System.Drawing.Size(116, 23)
        Me.dtpSignInDate.TabIndex = 6
        '
        'txtCountry
        '
        Me.txtCountry.Location = New System.Drawing.Point(547, 393)
        Me.txtCountry.Name = "txtCountry"
        Me.txtCountry.Size = New System.Drawing.Size(192, 23)
        Me.txtCountry.TabIndex = 22
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(483, 396)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(58, 16)
        Me.Label16.TabIndex = 46
        Me.Label16.Text = "Country"
        '
        'txtProvince
        '
        Me.txtProvince.Location = New System.Drawing.Point(547, 364)
        Me.txtProvince.Name = "txtProvince"
        Me.txtProvince.Size = New System.Drawing.Size(192, 23)
        Me.txtProvince.TabIndex = 21
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(478, 367)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(63, 16)
        Me.Label15.TabIndex = 44
        Me.Label15.Text = "Province"
        '
        'txtPostalCode
        '
        Me.txtPostalCode.Location = New System.Drawing.Point(547, 335)
        Me.txtPostalCode.Name = "txtPostalCode"
        Me.txtPostalCode.Size = New System.Drawing.Size(133, 23)
        Me.txtPostalCode.TabIndex = 20
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(456, 338)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(85, 16)
        Me.Label14.TabIndex = 42
        Me.Label14.Text = "Postal Code"
        '
        'txtCity
        '
        Me.txtCity.Location = New System.Drawing.Point(547, 306)
        Me.txtCity.Name = "txtCity"
        Me.txtCity.Size = New System.Drawing.Size(192, 23)
        Me.txtCity.TabIndex = 19
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(509, 309)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(32, 16)
        Me.Label13.TabIndex = 40
        Me.Label13.Text = "City"
        '
        'txtAddressLine2
        '
        Me.txtAddressLine2.Location = New System.Drawing.Point(547, 277)
        Me.txtAddressLine2.Name = "txtAddressLine2"
        Me.txtAddressLine2.Size = New System.Drawing.Size(307, 23)
        Me.txtAddressLine2.TabIndex = 18
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(438, 280)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(103, 16)
        Me.Label12.TabIndex = 38
        Me.Label12.Text = "Address Line 2"
        '
        'txtAddressLine1
        '
        Me.txtAddressLine1.Location = New System.Drawing.Point(547, 248)
        Me.txtAddressLine1.Name = "txtAddressLine1"
        Me.txtAddressLine1.Size = New System.Drawing.Size(307, 23)
        Me.txtAddressLine1.TabIndex = 17
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(438, 251)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(103, 16)
        Me.Label11.TabIndex = 36
        Me.Label11.Text = "Address Line 1"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(547, 219)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(231, 23)
        Me.txtEmail.TabIndex = 16
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(499, 222)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(42, 16)
        Me.Label10.TabIndex = 34
        Me.Label10.Text = "Email"
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(547, 190)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(231, 23)
        Me.txtPhone.TabIndex = 15
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(492, 193)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(49, 16)
        Me.Label9.TabIndex = 32
        Me.Label9.Text = "Phone"
        '
        'pnlGender
        '
        Me.pnlGender.Controls.Add(Me.rdoWoman)
        Me.pnlGender.Controls.Add(Me.rdoMan)
        Me.pnlGender.Controls.Add(Me.Label8)
        Me.pnlGender.Location = New System.Drawing.Point(432, 155)
        Me.pnlGender.Name = "pnlGender"
        Me.pnlGender.Size = New System.Drawing.Size(422, 29)
        Me.pnlGender.TabIndex = 13
        '
        'rdoWoman
        '
        Me.rdoWoman.AutoSize = True
        Me.rdoWoman.Location = New System.Drawing.Point(174, 7)
        Me.rdoWoman.Name = "rdoWoman"
        Me.rdoWoman.Size = New System.Drawing.Size(74, 20)
        Me.rdoWoman.TabIndex = 14
        Me.rdoWoman.Text = "Woman"
        Me.rdoWoman.UseVisualStyleBackColor = True
        '
        'rdoMan
        '
        Me.rdoMan.AutoSize = True
        Me.rdoMan.Checked = True
        Me.rdoMan.Location = New System.Drawing.Point(115, 7)
        Me.rdoMan.Name = "rdoMan"
        Me.rdoMan.Size = New System.Drawing.Size(53, 20)
        Me.rdoMan.TabIndex = 13
        Me.rdoMan.TabStop = True
        Me.rdoMan.Text = "Man"
        Me.rdoMan.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(53, 9)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 16)
        Me.Label8.TabIndex = 30
        Me.Label8.Text = "Gender"
        '
        'dtpBirthDate
        '
        Me.dtpBirthDate.CustomFormat = "dd.MM.yyyy"
        Me.dtpBirthDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpBirthDate.Location = New System.Drawing.Point(682, 126)
        Me.dtpBirthDate.Name = "dtpBirthDate"
        Me.dtpBirthDate.ShowUpDown = True
        Me.dtpBirthDate.Size = New System.Drawing.Size(96, 23)
        Me.dtpBirthDate.TabIndex = 12
        '
        'txtBirthPlace
        '
        Me.txtBirthPlace.Location = New System.Drawing.Point(547, 126)
        Me.txtBirthPlace.Name = "txtBirthPlace"
        Me.txtBirthPlace.Size = New System.Drawing.Size(129, 23)
        Me.txtBirthPlace.TabIndex = 11
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(504, 129)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(37, 16)
        Me.Label7.TabIndex = 27
        Me.Label7.Text = "Birth"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.Label6.ForeColor = System.Drawing.Color.Gray
        Me.Label6.Location = New System.Drawing.Point(544, 98)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(157, 14)
        Me.Label6.TabIndex = 26
        Me.Label6.Text = "ID Card / Credit Card / Passport"
        '
        'txtIDCat
        '
        Me.txtIDCat.Location = New System.Drawing.Point(547, 72)
        Me.txtIDCat.Name = "txtIDCat"
        Me.txtIDCat.Size = New System.Drawing.Size(109, 23)
        Me.txtIDCat.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(432, 49)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(109, 16)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "ID Card Number"
        '
        'txtIDNum
        '
        Me.txtIDNum.Location = New System.Drawing.Point(547, 46)
        Me.txtIDNum.Name = "txtIDNum"
        Me.txtIDNum.Size = New System.Drawing.Size(231, 23)
        Me.txtIDNum.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(44, 139)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 16)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "Nick Name"
        '
        'txtNickName
        '
        Me.txtNickName.Location = New System.Drawing.Point(124, 136)
        Me.txtNickName.Name = "txtNickName"
        Me.txtNickName.Size = New System.Drawing.Size(164, 23)
        Me.txtNickName.TabIndex = 4
        '
        'cboEmployeeTitleName
        '
        Me.cboEmployeeTitleName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboEmployeeTitleName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmployeeTitleName.FormattingEnabled = True
        Me.cboEmployeeTitleName.Items.AddRange(New Object() {"Supervisor", "Manager", "Staff"})
        Me.cboEmployeeTitleName.Location = New System.Drawing.Point(124, 174)
        Me.cboEmployeeTitleName.Name = "cboEmployeeTitleName"
        Me.cboEmployeeTitleName.Size = New System.Drawing.Size(279, 24)
        Me.cboEmployeeTitleName.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(84, 177)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 16)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Title"
        '
        'chkAutoEmployeeID
        '
        Me.chkAutoEmployeeID.AutoSize = True
        Me.chkAutoEmployeeID.Checked = True
        Me.chkAutoEmployeeID.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAutoEmployeeID.Location = New System.Drawing.Point(124, 75)
        Me.chkAutoEmployeeID.Name = "chkAutoEmployeeID"
        Me.chkAutoEmployeeID.Size = New System.Drawing.Size(65, 20)
        Me.chkAutoEmployeeID.TabIndex = 2
        Me.chkAutoEmployeeID.Text = "AUTO"
        Me.chkAutoEmployeeID.UseVisualStyleBackColor = True
        '
        'txtEmployeeCompanyID
        '
        Me.txtEmployeeCompanyID.Enabled = False
        Me.txtEmployeeCompanyID.Location = New System.Drawing.Point(124, 46)
        Me.txtEmployeeCompanyID.Name = "txtEmployeeCompanyID"
        Me.txtEmployeeCompanyID.Size = New System.Drawing.Size(164, 23)
        Me.txtEmployeeCompanyID.TabIndex = 1
        Me.txtEmployeeCompanyID.Text = "AUTO"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(24, 49)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 16)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Employee ID"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(47, 113)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Full Name"
        '
        'txtFullName
        '
        Me.txtFullName.Location = New System.Drawing.Point(124, 110)
        Me.txtFullName.Name = "txtFullName"
        Me.txtFullName.Size = New System.Drawing.Size(279, 23)
        Me.txtFullName.TabIndex = 3
        '
        'lblGroup2
        '
        Me.lblGroup2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblGroup2.Location = New System.Drawing.Point(421, 9)
        Me.lblGroup2.Name = "lblGroup2"
        Me.lblGroup2.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.lblGroup2.Size = New System.Drawing.Size(434, 26)
        Me.lblGroup2.TabIndex = 1
        Me.lblGroup2.Text = "Personal Information"
        Me.lblGroup2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmGroup1
        '
        Me.frmGroup1.BackColor = System.Drawing.Color.LightGray
        Me.frmGroup1.ForeColor = System.Drawing.Color.Black
        Me.frmGroup1.Location = New System.Drawing.Point(-1, 9)
        Me.frmGroup1.Name = "frmGroup1"
        Me.frmGroup1.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.frmGroup1.Size = New System.Drawing.Size(422, 26)
        Me.frmGroup1.TabIndex = 0
        Me.frmGroup1.Text = "Employee Data"
        Me.frmGroup1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSubTitle
        '
        Me.lblSubTitle.BackColor = System.Drawing.Color.SlateGray
        Me.lblSubTitle.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblSubTitle.ForeColor = System.Drawing.Color.White
        Me.lblSubTitle.Location = New System.Drawing.Point(-1, 34)
        Me.lblSubTitle.Name = "lblSubTitle"
        Me.lblSubTitle.Padding = New System.Windows.Forms.Padding(13, 0, 0, 0)
        Me.lblSubTitle.Size = New System.Drawing.Size(248, 22)
        Me.lblSubTitle.TabIndex = 4
        Me.lblSubTitle.Text = "FORM SUBTITLE"
        Me.lblSubTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.IndianRed
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Image = Global.IDEAPOS.My.Resources.Resources.close_button_16
        Me.btnClose.Location = New System.Drawing.Point(1221, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(34, 32)
        Me.btnClose.TabIndex = 14
        Me.btnClose.TabStop = False
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnSubmit
        '
        Me.btnSubmit.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.btnSubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSubmit.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubmit.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.btnSubmit.ForeColor = System.Drawing.Color.White
        Me.btnSubmit.Image = Global.IDEAPOS.My.Resources.Resources.save_icon_32
        Me.btnSubmit.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSubmit.Location = New System.Drawing.Point(142, 74)
        Me.btnSubmit.Name = "btnSubmit"
        Me.btnSubmit.Size = New System.Drawing.Size(105, 63)
        Me.btnSubmit.TabIndex = 26
        Me.btnSubmit.Text = "SUBMIT (F12)"
        Me.btnSubmit.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSubmit.UseVisualStyleBackColor = False
        '
        'btnNew
        '
        Me.btnNew.BackColor = System.Drawing.Color.LightSlateGray
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.btnNew.ForeColor = System.Drawing.Color.White
        Me.btnNew.Image = Global.IDEAPOS.My.Resources.Resources.edit_32
        Me.btnNew.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnNew.Location = New System.Drawing.Point(12, 74)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(73, 63)
        Me.btnNew.TabIndex = 18
        Me.btnNew.TabStop = False
        Me.btnNew.Text = "NEW"
        Me.btnNew.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnNew.UseVisualStyleBackColor = False
        '
        'DIEmployee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Silver
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(1255, 659)
        Me.Controls.Add(Me.btnSubmit)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.lblSubTitle)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Arial", 10.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "DIEmployee"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DIEmployee"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.pnlSignOut.ResumeLayout(False)
        Me.pnlSignOut.PerformLayout()
        Me.pnlGender.ResumeLayout(False)
        Me.pnlGender.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents lblSubTitle As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents frmGroup1 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblGroup2 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents btnSubmit As System.Windows.Forms.Button
    Friend WithEvents txtFullName As System.Windows.Forms.TextBox
    Friend WithEvents txtEmployeeCompanyID As System.Windows.Forms.TextBox
    Friend WithEvents chkAutoEmployeeID As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeTitleName As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNickName As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtIDCat As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtIDNum As System.Windows.Forms.TextBox
    Friend WithEvents dtpBirthDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtBirthPlace As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents pnlGender As System.Windows.Forms.Panel
    Friend WithEvents rdoWoman As System.Windows.Forms.RadioButton
    Friend WithEvents rdoMan As System.Windows.Forms.RadioButton
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtPhone As System.Windows.Forms.TextBox
    Friend WithEvents txtProvince As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtPostalCode As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtCity As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtAddressLine2 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtAddressLine1 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtCountry As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents dtpSignInDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnlSignOut As System.Windows.Forms.Panel
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents dtpSignOutDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkSetSignOut As System.Windows.Forms.CheckBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents chkAppUserActive As System.Windows.Forms.CheckBox
    Friend WithEvents txtAppUserDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtAppUserName As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents lblAppUserNameInfo As System.Windows.Forms.Label

End Class
