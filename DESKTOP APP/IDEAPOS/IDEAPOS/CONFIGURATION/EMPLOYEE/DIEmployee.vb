﻿Imports System.Windows.Forms

Public Class DIEmployee
    Friend modeInsert As Boolean = True
    Friend updateID As Integer = 0

    'Local Need
    Dim dsUpdate As New DataSet

    Dim dsAppUser As New DataSet
    Dim dsEmployeeTitle As New DataSet


    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub _InputFormTemplate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If applyDIConfig(Me.Name, lblTitle, lblSubTitle) Then
            isiDsAppUser()
            isiDsEmployeeTitle()
        End If

        If Not modeInsert Then
            'modeUpdate
            loadUpdate()
        Else
            btnNew_Click(Nothing, Nothing)
        End If

        Me.KeyPreview = True
        AddHandler Me.KeyDown, AddressOf KeyDownHandler
        AddHandler Me.KeyUp, AddressOf KeyUpHandler
    End Sub


    Private Sub KeyUpHandler(ByVal o As Object, ByVal e As KeyEventArgs)
        'e.SuppressKeyPress = True
        If e.KeyCode = Keys.F12 Then
            btnSubmit_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub KeyDownHandler(ByVal o As Object, ByVal e As KeyEventArgs)
        'e.SuppressKeyPress = True
    End Sub

    Private Sub loadUpdate()
        pastikanKoneksi()
        query = "select  " & _
                "EmployeeID, " & _
                "EmployeeCompanyID, " & _
                "EmployeeCompanyIDNum, " & _
                "EmployeeTitleID, " & _
                "IDNum, " & _
                "IDCat, " & _
                "FullName, " & _
                "NickName, " & _
                "BirthPlace, " & _
                "BirthDay, " & _
                "Gender, " & _
                "if(Gender = 1, 'Man', 'Woman') GenderValue, " & _
                "Phone, " & _
                "Email, " & _
                "AddressLine1, " & _
                "AddressLine2, " & _
                "City, " & _
                "PostalCode, " & _
                "Province, " & _
                "Country, " & _
                "SignInDate, " & _
                "if(SignOutDate is null, '0000-00-00 00:00:00', SignOutDate) SignOutDate, " & _
                "Active, " & _
                "EmployeeTitleName, " & _
                "if(AppUserID is null, 0, AppUserID) AppUserID, " & _
                "if(AppUserName is null, '', AppUserName) AppUserName, " & _
                "if(AppUserPassword is null, '', AppUserPassword) AppUserPassword, " & _
                "if(AppUserDesc is null, '', AppUserDesc) AppUserDesc, " & _
                "if(DefaultUser is null, 0, DefaultUser) DefaultUser " & _
                "from vShowAllEmployee " & _
                "where EmployeeID = @EmployeeID "
        mycmd = New MySqlCommand(query, mycon)
        mycmd.Parameters.AddWithValue("@EmployeeID", updateID)
        dsUpdate = New DataSet
        Try
            myda = New MySqlDataAdapter(mycmd)
            myda.Fill(dsUpdate, "data")

            'Employee Data
            chkAutoEmployeeID.Enabled = False
            txtEmployeeCompanyID.Text = dsUpdate.Tables("data").Rows(0)("EmployeeCompanyID")
            txtFullName.Text = dsUpdate.Tables("data").Rows(0)("FullName")
            txtNickName.Text = dsUpdate.Tables("data").Rows(0)("NickName")
            cboEmployeeTitleName.Text = dsUpdate.Tables("data").Rows(0)("EmployeeTitleName")
            dtpSignInDate.Value = dsUpdate.Tables("data").Rows(0)("SignInDate")
            chkSetSignOut.Checked = Not dsUpdate.Tables("data").Rows(0)("Active")
            If Not dsUpdate.Tables("data").Rows(0)("Active") Then
                dtpSignOutDate.Value = dsUpdate.Tables("data").Rows(0)("SignOutDate")
            End If

            'Personal Information
            txtIDNum.Text = dsUpdate.Tables("data").Rows(0)("IDNum")
            txtIDCat.Text = dsUpdate.Tables("data").Rows(0)("IDCat")
            txtBirthPlace.Text = dsUpdate.Tables("data").Rows(0)("BirthPlace")
            dtpBirthDate.Value = dsUpdate.Tables("data").Rows(0)("BirthDay")
            rdoMan.Checked = dsUpdate.Tables("data").Rows(0)("Gender")
            txtPhone.Text = dsUpdate.Tables("data").Rows(0)("Phone")
            txtEmail.Text = dsUpdate.Tables("data").Rows(0)("Email")
            txtAddressLine1.Text = dsUpdate.Tables("data").Rows(0)("AddressLine1")
            txtAddressLine2.Text = dsUpdate.Tables("data").Rows(0)("AddressLine2")
            txtCity.Text = dsUpdate.Tables("data").Rows(0)("City")
            txtPostalCode.Text = dsUpdate.Tables("data").Rows(0)("PostalCode")
            txtProvince.Text = dsUpdate.Tables("data").Rows(0)("Province")
            txtCountry.Text = dsUpdate.Tables("data").Rows(0)("Country")
            txtAppUserName.Text = dsUpdate.Tables("data").Rows(0)("AppUserName")
            txtAppUserDesc.Text = dsUpdate.Tables("data").Rows(0)("AppUserDesc")
            lblAppUserNameInfo.Text = ""
            lblAppUserNameInfo.ForeColor = Color.Green
            lblAppUserNameInfo.Visible = True
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub chkAutoEmployeeID_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoEmployeeID.CheckedChanged
        If chkAutoEmployeeID.Checked Then
            txtEmployeeCompanyID.Enabled = False
            txtEmployeeCompanyID.Text = "AUTO"
        Else
            txtEmployeeCompanyID.Enabled = True
            txtEmployeeCompanyID.Text = ""
        End If
    End Sub

    Private Sub isiDsAppUser()
        pastikanKoneksi()
        query = "select * from AppUser "
        mycmd = New MySqlCommand(query, mycon)
        Try
            dsAppUser = New DataSet
            myda = New MySqlDataAdapter(query, mycon)
            myda.Fill(dsAppUser, "data")
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub isiDsEmployeeTitle()
        pastikanKoneksi()
        query = "select * from EmployeeTitle "
        mycmd = New MySqlCommand(query, mycon)
        Try
            dsEmployeeTitle = New DataSet
            myda = New MySqlDataAdapter(query, mycon)
            myda.Fill(dsEmployeeTitle, "data")
        Catch ex As Exception
            MsgBox(ex.ToString)
            Exit Sub
        End Try
        cboEmployeeTitleName.Items.Clear()
        For i As Integer = 0 To dsEmployeeTitle.Tables("data").Rows.Count - 1
            cboEmployeeTitleName.Items.Add(dsEmployeeTitle.Tables("data").Rows(i)("EmployeeTitleName").ToString)
        Next
    End Sub

    Private Sub chkSetSignOut_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSetSignOut.CheckedChanged
        pnlSignOut.Visible = chkSetSignOut.Checked
    End Sub

    Private Sub txtAppUserName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAppUserName.TextChanged
        If modeInsert Then
            If txtAppUserName.Text.Trim.Length > 0 Then
                If dsAppUser.Tables("data").Select("AppUserName = '" & txtAppUserName.Text.Trim & "'").Length > 0 Then
                    lblAppUserNameInfo.Text = "Not Available"
                    lblAppUserNameInfo.ForeColor = Color.Maroon
                    lblAppUserNameInfo.Visible = True
                Else
                    lblAppUserNameInfo.Text = "OK"
                    lblAppUserNameInfo.ForeColor = Color.Green
                    lblAppUserNameInfo.Visible = True
                End If
            Else
                lblAppUserNameInfo.Visible = False
            End If
        Else
            If txtAppUserName.Text.Trim.Length > 0 Then
                If dsAppUser.Tables("data").Select("AppUserName = '" & txtAppUserName.Text.Trim & "' and AppUserName <> '" & dsUpdate.Tables("data").Rows(0)("AppUserName") & "'").Length > 0 Then
                    lblAppUserNameInfo.Text = "Not Available"
                    lblAppUserNameInfo.ForeColor = Color.Maroon
                    lblAppUserNameInfo.Visible = True
                Else
                    lblAppUserNameInfo.Text = "OK"
                    lblAppUserNameInfo.ForeColor = Color.Green
                    lblAppUserNameInfo.Visible = True
                End If
            Else
                lblAppUserNameInfo.Visible = False
            End If
        End If

    End Sub



    Private Function submitInsert()
        Dim localDs As New DataSet
        Dim AppUserID As Integer = 0
        Dim EmployeeTitleID As Integer = 0
        Dim EmployeeCompanyIDNum As Integer = 0
        pastikanKoneksi()
        'APPUSER
        If txtAppUserName.Text.Trim.Length > 0 Then
            'Check AppUserID (if exists)
            If Not lblAppUserNameInfo.Text.ToLower.Contains("not") Then
                'Create New AppUser
                query = "select max(AppUserID) + 1 AppUserID from appuser "
                mycmd = New MySqlCommand(query, mycon)
                Try
                    myda = New MySqlDataAdapter(mycmd)
                    myda.Fill(localDs, "data")
                    AppUserID = IIf(localDs.Tables("data").Rows.Count > 0, localDs.Tables("data").Rows(0)("AppUserID"), 1)
                Catch ex As Exception
                    MsgBox("1" & vbNewLine & ex.ToString)
                    Return 0
                End Try

                query = "insert into appuser " & _
                        "set " & _
                        "AppUserID = @AppUserID, " & _
                        "AppUserName = @AppUserName, " & _
                        "AppUserPassword = md5(@AppUserPassword), " & _
                        "AppUserDesc = @AppUserDesc, " & _
                        "DefaultUser = @DefaultUser, " & _
                        "Active = @Active "
                mycmd = New MySqlCommand(query, mycon)
                mycmd.Parameters.AddWithValue("@AppUserID", AppUserID)
                mycmd.Parameters.AddWithValue("@AppUserName", txtAppUserName.Text.Trim)
                mycmd.Parameters.AddWithValue("@AppUserPassword", "123456")
                mycmd.Parameters.AddWithValue("@AppUserDesc", txtAppUserDesc.Text.Trim)
                mycmd.Parameters.AddWithValue("@DefaultUser", False)
                mycmd.Parameters.AddWithValue("@Active", chkAppUserActive.Checked)
                Try
                    mycmd.ExecuteNonQuery()
                Catch ex As Exception
                    MsgBox("2" & vbNewLine & ex.ToString)
                    Return 0
                End Try
            Else
                Return 0
            End If
        End If

        'Check EmployeeTitleID (if exists)
        If cboEmployeeTitleName.SelectedIndex > -1 Then
            'Exists
            EmployeeTitleID = dsEmployeeTitle.Tables("data").Rows(cboEmployeeTitleName.SelectedIndex)("EmployeeTitleID")
        Else
            'Create New EmployeeTitle
            query = "select max(EmployeeTitleID) + 1 EmployeeTitleID from employeetitle "
            mycmd = New MySqlCommand(query, mycon)
            Try
                localDs = New DataSet
                myda = New MySqlDataAdapter(mycmd)
                myda.Fill(localDs, "data")
                EmployeeTitleID = IIf(localDs.Tables("data").Rows.Count > 0, localDs.Tables("data").Rows(0)("EmployeeTitleID"), 1)
            Catch ex As Exception
                MsgBox("3" & vbNewLine & ex.ToString)
                Return 0
            End Try

            query = "insert into employeetitle " & _
                    "set " & _
                    "EmployeeTitleID = @EmployeeTitleID, " & _
                    "EmployeeTitleName = @EmployeeTitleName, " & _
                    "Active = @Active "
            mycmd = New MySqlCommand(query, mycon)
            mycmd.Parameters.AddWithValue("@EmployeeTitleID", EmployeeTitleID)
            mycmd.Parameters.AddWithValue("@EmployeeTitleName", cboEmployeeTitleName.Text)
            mycmd.Parameters.AddWithValue("@Active", True)
            Try
                mycmd.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox("4" & vbNewLine & ex.ToString)
                Return 0
            End Try
        End If

        'FINAL
        'Get AUTO EmployeeCompanyNum
        If chkAutoEmployeeID.Checked Then
            query = "select max(EmployeeCompanyIDNum) + 1 EmployeeCompanyIDNum from employee "
            mycmd = New MySqlCommand(query, mycon)
            Try
                localDs = New DataSet
                myda = New MySqlDataAdapter(mycmd)
                myda.Fill(localDs, "data")
                'MsgBox(localDs.Tables("data").Rows(0)("EmployeeCompanyIDNum").ToString)
                EmployeeCompanyIDNum = IIf(localDs.Tables("data").Rows.Count > 0, localDs.Tables("data").Rows(0)("EmployeeCompanyIDNum"), 1)
            Catch ex As Exception
                MsgBox("5" & vbNewLine & ex.ToString)
                Return 0
            End Try
        End If

        query = "insert into employee " & _
                "set " & _
                "EmployeeCompanyID = @EmployeeCompanyID, " & _
                "EmployeeCompanyIDNum = @EmployeeCompanyIDNum, " & _
                "EmployeeTitleID = @EmployeeTitleID, " & _
                "AppUserID = @AppUserID, " & _
                "IDNum = @IDNum, " & _
                "IDCat = @IDCat, " & _
                "FullName = @FullName, " & _
                "NickName = @NickName, " & _
                "BirthPlace = @BirthPlace, " & _
                "BirthDay = @BirthDay, " & _
                "Gender = @Gender, " & _
                "Phone = @Phone, " & _
                "Email = @Email, " & _
                "AddressLine1 = @AddressLine1, " & _
                "AddressLine2 = @AddressLine2, " & _
                "City = @City, " & _
                "PostalCode = @PostalCode, " & _
                "Province = @Province, " & _
                "Country = @Country, " & _
                "SignInDate = @SignInDate, " & _
                "SignOutDate = @SignOutDate, " & _
                "Active = @Active "
        mycmd = New MySqlCommand(query, mycon)
        If chkAutoEmployeeID.Checked Then
            mycmd.Parameters.AddWithValue("@EmployeeCompanyID", "E-" & Format(EmployeeCompanyIDNum, "00000"))
        Else
            mycmd.Parameters.AddWithValue("@EmployeeCompanyID", txtEmployeeCompanyID.Text.Trim)
        End If
        mycmd.Parameters.AddWithValue("@EmployeeCompanyIDNum", EmployeeCompanyIDNum)
        mycmd.Parameters.AddWithValue("@EmployeeTitleID", EmployeeTitleID)
        If txtAppUserName.Text.Trim.Length = 0 Then
            mycmd.Parameters.AddWithValue("@AppUserID", DBNull.Value)
        Else
            mycmd.Parameters.AddWithValue("@AppUserID", AppUserID)
        End If
        mycmd.Parameters.AddWithValue("@IDNum", txtIDNum.Text)
        mycmd.Parameters.AddWithValue("@IDCat", txtIDCat.Text)
        mycmd.Parameters.AddWithValue("@FullName", txtFullName.Text)
        mycmd.Parameters.AddWithValue("@NickName", txtNickName.Text)
        mycmd.Parameters.AddWithValue("@BirthPlace", txtBirthPlace.Text)
        mycmd.Parameters.AddWithValue("@BirthDay", dtpBirthDate.Value.ToString("yyyy-MM-dd"))
        mycmd.Parameters.AddWithValue("@Gender", rdoMan.Checked)
        mycmd.Parameters.AddWithValue("@Phone", txtPhone.Text)
        mycmd.Parameters.AddWithValue("@Email", txtEmail.Text)
        mycmd.Parameters.AddWithValue("@AddressLine1", txtAddressLine1.Text)
        mycmd.Parameters.AddWithValue("@AddressLine2", txtAddressLine2.Text)
        mycmd.Parameters.AddWithValue("@City", txtCity.Text)
        mycmd.Parameters.AddWithValue("@PostalCode", txtPostalCode.Text)
        mycmd.Parameters.AddWithValue("@Province", txtProvince.Text)
        mycmd.Parameters.AddWithValue("@Country", txtCountry.Text)
        mycmd.Parameters.AddWithValue("@SignInDate", dtpSignInDate.Value.ToString("yyyy-MM-dd"))
        mycmd.Parameters.AddWithValue("@SignOutDate", IIf(chkSetSignOut.Checked, dtpSignOutDate.Value.ToString("yyyy-MM-dd"), "0000-00-00"))
        mycmd.Parameters.AddWithValue("@Active", Not chkSetSignOut.Checked)
        Try
            myda = New MySqlDataAdapter(mycmd)
            myda.Fill(localDs, "data")
        Catch ex As Exception
            MsgBox("6" & vbNewLine & ex.ToString)
            Return 0
        End Try
        isiDsAppUser()
        isiDsEmployeeTitle()
        Return True
    End Function

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        modeInsert = True
        updateID = 0
        resetForm(pnlMain)
        cboEmployeeTitleName.Text = ""
        If chkAutoEmployeeID.Checked Then
            txtEmployeeCompanyID.Text = "AUTO"
        Else
            txtEmployeeCompanyID.Text = ""
        End If
        txtEmployeeCompanyID.Focus()
        'MsgBox("New")
    End Sub

    Private Sub btnOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MsgBox("Open")
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MsgBox("Delete")
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If cboEmployeeTitleName.Text.Trim.Length > 0 And txtEmployeeCompanyID.Text.Trim.Length > 0 Then
            If modeInsert Then
                If submitInsert() Then
                    btnNew_Click(Nothing, Nothing)
                Else
                    MsgBox("Gagal")
                End If
            Else

            End If
        Else
            MessageBox.Show("")
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#Region "Form Moving"
    'FORM MOVING
    Public MoveForm As Boolean
    Public MoveForm_MousePosition As Point
    Public Sub MoveForm_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _
    MyBase.MouseDown ' Add more handles here (Example: PictureBox1.MouseDown)
        If e.Button = MouseButtons.Left Then
            MoveForm = True
            Me.Cursor = Cursors.NoMove2D
            MoveForm_MousePosition = e.Location
        End If
    End Sub

    Public Sub MoveForm_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _
    MyBase.MouseMove ' Add more handles here (Example: PictureBox1.MouseMove)
        If MoveForm Then
            Me.Location = Me.Location + (e.Location - MoveForm_MousePosition)
        End If
    End Sub

    Public Sub MoveForm_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles _
    MyBase.MouseUp ' Add more handles here (Example: PictureBox1.MouseUp)
        If e.Button = MouseButtons.Left Then
            MoveForm = False
            Me.Cursor = Cursors.Default
        End If
    End Sub
#End Region
#Region "Maximize"
    'Private Sub _InputFormTemplate_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.DoubleClick
    '    If Me.WindowState = FormWindowState.Normal Then
    '        Me.WindowState = FormWindowState.Maximized
    '    Else
    '        Me.WindowState = FormWindowState.Normal
    '    End If
    'End Sub
#End Region
End Class
